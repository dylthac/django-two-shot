from django.urls import path

from receipts.views import (
    receipt_list,
    create_receipt,
    show_accounts,
    show_expense_category,
)


urlpatterns = [
    path("", receipt_list, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("accounts/", show_accounts, name="list_accounts"),
    path("categories/", show_expense_category, name="list_category"),
]
