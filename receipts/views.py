from django.shortcuts import render, redirect
from receipts.forms import ReceiptForm


from receipts.models import Account, ExpenseCategory, Receipt
from django.contrib.auth.decorators import login_required


@login_required
def receipt_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {"receipt_list": receipts}
    return render(request, "receipts/list.html", context)


def show_accounts(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {"account_list": accounts}
    return render(request, "accounts/list.html", context)


def show_expense_category(request):
    expense_categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {"expense_category_list": expense_categories}
    return render(request, "expense_cat/list.html", context)


def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()

    context = {"form": form}
    return render(request, "receipts/create.html", context)
